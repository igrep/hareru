# Hareru - 🌞 Sunny Haskell Relational Record (HRR) wrapper 🌞 -

This package provides wrapper functions focused on more rapid development
with [Haskell Relational Record](http://khibino.github.io/haskell-relational-record/).  
Some of the outcome might be taken in the upstream package HRR.
