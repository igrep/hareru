module Database.Hareru.Types where


import           Database.Record (ToSql, FromSql)
import           Database.Hareru.Wrapper (Select, Insert)
import           Database.HDBC
                   ( IConnection
                   , SqlValue
                   )


type ToSqlValue = ToSql SqlValue

type FromSqlValue = FromSql SqlValue


class MonadRdbTransaction m where
  execSelect
    :: (ToSqlValue p, FromSqlValue a, Show p, GetConnection m)
    => Select p a
    -> p -> m [a]
  execInsertByChunks
    :: (ToSqlValue p, Show p, GetConnection m)
    => Insert p -> [p] -> m [Integer]
  withConnectionGetter
    :: (IConnection conn, GetConnection n)
    => m conn
    -> n a
    -> m a


class GetConnection m where
  getConnection :: IConnection conn => m conn
