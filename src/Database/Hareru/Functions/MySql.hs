{-# OPTIONS_GHC -fno-warn-redundant-constraints #-}

module Database.Hareru.Functions.MySql
  ( groupConcat
  , groupConcatMaybe
  , ifnull
  ) where


import           Data.String (IsString)
import           Database.Relational.Context (Flat)
import           Database.Relational.Projectable (unsafeProjectSql')
import           Database.Relational.Record
                   ( Record
                   , just
                   , unsafeStringSql
                   )
import           Database.Relational.Projectable.Unsafe
                   ( AggregatedContext
                   , SqlContext
                   )
import           Language.SQL.Keyword (word, (<++>))


groupConcat
  :: (AggregatedContext ac, SqlContext ac, IsString a)
  => Record Flat a -> Record ac a -> Record ac (Maybe a)
groupConcat mbr = groupConcatMaybe (just mbr)


groupConcatMaybe
  :: (AggregatedContext ac, SqlContext ac, IsString a)
  => Record Flat (Maybe a) -> Record ac a -> Record ac (Maybe a)
groupConcatMaybe mbr sep =
  unsafeProjectSql' $
    word "GROUP_CONCAT("
      <++> unsafeStringSql mbr
      <++> word ", "
      <++> unsafeStringSql sep
      <++> word ")"


ifnull :: SqlContext ac => Record ac (Maybe a) -> Record ac a -> Record ac a
ifnull mbr ifN =
  unsafeProjectSql' $
    word "IFNULL("
      <++> unsafeStringSql mbr
      <++> word ", "
      <++> unsafeStringSql ifN
      <++> word ")"
