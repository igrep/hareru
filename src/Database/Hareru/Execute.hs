module Database.Hareru.Execute where


import           Database.Record (ToSql, FromSql)
import           Database.Relational (Relation, relationalQuery)
import           Database.HDBC.Record (runQuery')
import           Database.HDBC
                   ( IConnection
                   , SqlValue
                   )


runRelationIo
  :: (ToSql SqlValue p, IConnection conn, FromSql SqlValue a)
  => conn
  -> Relation p a
  -> p -> IO [a]
runRelationIo conn q = runQuery' conn (relationalQuery q)
