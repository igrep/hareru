module Database.Hareru.Wrapper
  ( module Database.HDBC
  , module Database.HDBC.Query.TH
  , module Database.HDBC.Record
  , module Database.HDBC.Session
  , module Database.Record
  , module Database.Relational
  , module Database.Relational.OverloadedInstances

  , select
  , aggregatedSelect
  , selectP
  , aggregatedSelectP

  , QuerySimpleP
  , QueryAggregateP

  , Select
  , InsertSelect

  , from
  , maybeFrom
  , valueFromSelect
  , valueFromSelectP
  , listFromSelect
  , listFromSelectP

  , (|$|)
  , (|*|)

  , runSelect
  , runInsertSelect
  ) where

import           Database.HDBC hiding (execute, finish, run)
import           Database.HDBC.Query.TH hiding (makeRelationalRecord)
import           Database.HDBC.Record hiding (execute, finish, runQuery, runInsertQuery)
import qualified Database.HDBC.Record as HRR
import           Database.HDBC.Session
import           Database.Record
import           Database.Relational hiding
                   ( Relation

                   , relation
                   , relation'
                   , aggregateRelation
                   , aggregateRelation'

                   , query
                   , queryMaybe

                   , InsertQuery

                   , queryScalar
                   , queryScalar'
                   , queryList
                   , queryList'

                   , runQuery

                   , unique -- TODO: Avoid conflict with Database.Record.unique
                   )

import           Data.Functor.ProductIsomorphic ((|$|), (|*|))
import qualified Database.Relational as HRR
import qualified Database.Relational.OverloadedInstances ()

type Select = HRR.Relation

type InsertSelect = HRR.InsertQuery

type QuerySimpleP p r = HRR.QuerySimple (HRR.PlaceHolders p, HRR.Record HRR.Flat r)

type QueryAggregateP p r =
  HRR.QueryAggregate (HRR.PlaceHolders p, HRR.Record HRR.Aggregated r)


select :: HRR.QuerySimple (HRR.Record HRR.Flat r) -> Select () r
select = HRR.relation


selectP :: QuerySimpleP p r -> Select p r
selectP = HRR.relation'


aggregatedSelect :: HRR.QueryAggregate (HRR.Record HRR.Aggregated r) -> Select () r
aggregatedSelect = HRR.aggregateRelation


aggregatedSelectP :: QueryAggregateP p r -> Select p r
aggregatedSelectP = HRR.aggregateRelation'


from
  :: (HRR.MonadQualify HRR.ConfigureQuery m, HRR.MonadQuery m)
  => Select () r -> m (HRR.Record HRR.Flat r)
from = HRR.query


maybeFrom
  :: (HRR.MonadQualify HRR.ConfigureQuery m, HRR.MonadQuery m)
  => Select () r -> m (HRR.Record HRR.Flat (Maybe r))
maybeFrom = HRR.queryMaybe


valueFromSelect
  :: (MonadQualify ConfigureQuery m, ScalarDegree r)
  => UniqueRelation () c r -> m (Record c (Maybe r))
valueFromSelect = HRR.queryScalar


valueFromSelectP
  :: (MonadQualify ConfigureQuery m, ScalarDegree r)
  => UniqueRelation p c r -> m (PlaceHolders p, Record c (Maybe r))
valueFromSelectP = HRR.queryScalar'


listFromSelect
  :: MonadQualify ConfigureQuery m
  => Select () r -> m (RecordList (Record c) r)
listFromSelect = HRR.queryList


listFromSelectP
  :: MonadQualify ConfigureQuery m
  => Select p r -> m (PlaceHolders p, RecordList (Record c) r)
listFromSelectP = HRR.queryList'


runSelect :: (ToSql SqlValue p,
               IConnection conn,
               FromSql SqlValue a) =>
               conn -> Select p a -> p -> IO [a]
runSelect conn q = HRR.runQuery conn (HRR.relationalQuery q)

runInsertSelect :: (IConnection conn, ToSql SqlValue p) => conn -> InsertSelect p -> p -> IO Integer
runInsertSelect = HRR.runInsertQuery
